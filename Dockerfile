FROM registry.access.redhat.com/ubi8

RUN [ $(uname -m) == x86_64 ] && ARCH=amd64 || ARCH=$(uname -m) && echo $ARCH && \
    cd /tmp && \
    curl -o helm.tar.gz https://get.helm.sh/helm-v3.7.1-linux-${ARCH}.tar.gz && \
    tar zxvf helm.tar.gz && \
    mv linux-*/helm /usr/local/bin/helm && chmod +x /usr/local/bin/helm && \
    rm -rf /tmp/* && \
    ls -l /usr/local/bin

ENTRYPOINT ["/usr/local/bin/helm"]
